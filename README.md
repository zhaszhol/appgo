**Сборка и деплой**

1. Сборка проекта на Kaniko, используется executor kubernetes
2. Деплоймет и роллбак проекта на Helm, используется executor shell

 Правильно ли использовать два екзекютора одновременно? 
 И вообще есть какое нибудь правило, если проект на кубере использовать только екзекютор кубера, докер соответственно экзекютор докер ?


**Что нужно для улучшения**

1. Для отказоустойчивости добавил бы в манифест деплоймента пробы и афинити правила. 
2. Отчистить проект от ненужных файлов и структурировать файлы правильно.

п.с. Это мои первый проект который я здал на проверку. Извините за небрежный вид проекта. Посмотрел вчера разбор задач, там разбирали работы Ильи, и подумал тоже расписать все, исправить не было времени.
