FROM golang:1.12.0-alpine3.9 AS builder
RUN mkdir /app
WORKDIR /app
COPY . ./
RUN apk add git && go mod download
RUN CGO_ENABLED=0 GOOS=linux go build -o main ./...

FROM alpine:latest AS production
COPY --from=builder /app .
EXPOSE 8080
CMD [ "./main" ]
